const calc = require('./random');

test('test random', () => {
  expect(calc.genRandomNumber(1, 10)).toBeGreaterThan(0);
  expect(calc.genRandomNumber(1, 10)).toBeLessThan(11);
});