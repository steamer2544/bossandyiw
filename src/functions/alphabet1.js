export function generateRandomAlphabet1() {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  const index = Math.floor(Math.random() * alphabet.length);
  return alphabet[index];
}
