const alpha = require('./alphabet');

test('generates a random alphabet', () => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  const randomAlphabet = alpha.generateRandomAlphabet();
  expect(alphabet.includes(randomAlphabet)).toBe(true);
});


